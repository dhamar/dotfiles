call plug#begin('~/.vim/plugged')
Plug 'sheerun/vim-polyglot'
Plug 'fatih/vim-go'
call plug#end()
syntax on
filetype plugin on
filetype indent on
set hidden
set lazyredraw
set showmode
set wildmenu
set relativenumber number
set ai
set hlsearch
set ruler
set background=dark
highlight Comment ctermfg=grey
